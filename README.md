# PWC Salesforce Grant Management #

Before migrating data to another org, enable digital experiences in the destination org and enter the same domain name that you used in your sandbox org to avoid getting an error.
For each Experience Cloud site, the network component has a unique name and URL path prefix. 

When you retrieve the network component, the generated XML file name is based on the name of the network. 

When migrating, the API looks at the file name and if it exists, updates the site. If it doesn’t exist, the API creates a site. If someone changes the site name in the sandbox and then tries to migrate, they see an error because the API is trying to create a site with the existing path prefix.
Examine the XML file for CustomSite to make sure that all dependencies are brought over. 

If any are missing, explicitly state them in the XML file.
In addition to the Network, CustomSite, and ExperienceBundle components, include all the other components required by your site, such as custom objects, custom fields, custom Lightning components, and Apex classes.
To deploy the Network and Profile components using unlocked packages, create a separate unlocked package for each component and deploy them individually.
When deploying an Experience Builder site with ExperienceBundle, ensure that the SiteDotCom type isn’t included in the manifest file.

If you rename a site in Administration | Settings, make sure that the source and target sites have matching values for the picassoSite and site attributes in the Network component.

If there are any changes to the guest user profile, include the profile as part of the site migration.

When you migrate user profiles, users are added to the site in the production org. Emails are then sent to members in the same way as for any new site.
During deployment, make sure that the NavigationMenu developer name in the target org is the same as the developer name in the source org.
If the containerType is CommunityTemplateDefinition, you can’t update an existing NavigationMenu via Metadata API.

Deploying the navigation menu with additional menu items deletes any translations applied to existing menu items in the target environment.
You can’t deploy to a target org that’s using an earlier release version. For example, if your source org is on Summer ’19 (API version 46.0), you can’t deploy to a target org on Spring ’19 (API version 45.0).
ExperienceBundle doesn’t support retrieving and deploying across different API versions. If you’re trying to upgrade ExperienceBundle metadata from an earlier API version to a later one—for example, from API version 48.0 to 49.0—take the following steps:
Set the API version in the package.xml manifest file to 48.0 and deploy the package.

Then, set the API version in package.xml to 49.0.

Retrieve the package to get the latest ExperienceBundle updates.

### What is this repository for? ###

This is repository for Grant management.

### How do I get set up? ###

https://help.salesforce.com/articleView?id=networks_migrating_from_sandbox.htm&type=0

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact