({
    doInit : function(component, event, helper) {
        component.set("v.queryLimit", "5");
        helper.doInitHelper(component, event);
    },
    
    viewMoreRecords : function(component, event, helper) {
        component.set("v.queryLimit", "50000");
        helper.doInitHelper(component, event);
    },
    
    viewLessRecords : function(component, event, helper) {
        component.set("v.queryLimit", "5");
        helper.doInitHelper(component, event);
    }
})