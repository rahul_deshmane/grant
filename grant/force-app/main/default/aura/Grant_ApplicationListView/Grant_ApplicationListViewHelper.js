({
    doInitHelper : function(component, event) {
        component.set("v.showSpinner", true);
        let limit = component.get("v.queryLimit");
        let action = component.get("c.getGrantRecords");
        action.setParams({
            'recLimit' : limit
        });
        action.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                let records = JSON.parse(response.getReturnValue());
                console.log('@@@ Records : ', records);
                component.set("v.recordList", records);
                console.log('Limit : ', limit);
                if(limit == "50000") {
                    component.set("v.showViewLess", true);
                } else {
                    component.set("v.showViewLess", false);
                }
                component.set("v.showSpinner", false);
            } else {
                component.set("v.showSpinner", false);
                console.log("Error");
            }
        });
        $A.enqueueAction(action);
    }
})