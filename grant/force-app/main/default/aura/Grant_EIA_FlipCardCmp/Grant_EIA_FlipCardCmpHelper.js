({
	fetchTasks : function(component,event) {
        var action = component.get("c.getAllTasks");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               component.set("v.taskList", response.getReturnValue());
                console.log(response);
                
                 var pageSize = component.get("v.pageSize");
                // hold all the records into an attribute named "taskList"
                component.set('v.taskList', response.getReturnValue());
                // get size of all the records and then hold into an attribute "totalRecords"
                component.set("v.totalRecords", component.get("v.taskList").length);
                // set star as 0
                component.set("v.startPage",0);
                 
                component.set("v.endPage",pageSize-1);
                var PaginationList = [];
                for(var i=0; i< pageSize; i++){
                    if(component.get("v.taskList").length> i)
                        PaginationList.push(response.getReturnValue()[i]);    
                }

                //var agreements = PaginationList; before
                var agreements = component.get("v.taskList");
                // agreements.forEach(function(agreement){
                // agreement.displayClass = (agreement.Status = 'Additional Info Required' ? 'Red' : 
                //             agreement.Status = 'EPA Recommendation required' ? 'White' : 
                //             agreement.Status = 'Comment on EPA.'  ? 'Blue' : 'White' );
                // });
                console.log('@@@ agreements before'+JSON.stringify(agreements));
                var counts=0;
                for(counts=0;counts<agreements.length;counts++){
                    if(agreements[counts].Status =='Additional Info Required' && counts<1){

                        agreements[counts].displayClass='Red';
                    }else if(counts==1){
                        agreements[counts].displayClass='Blue';
                    }else if(agreements[counts].Status =='Comment on EPA.'){

                        agreements[counts].displayClass='White';
                    }else{

                        agreements[counts].displayClass='White';
                    }


                }
                console.log('@@@ agreements'+JSON.stringify(agreements));
                const pageItems = agreements.slice(0, pageSize);
                component.set("v.PaginationList", pageItems);                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    next : function(component, event){
        var sObjectList = component.get("v.taskList");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var Paginationlist = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                Paginationlist.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.PaginationList', Paginationlist);
    },
    
    previous : function(component, event){
        var sObjectList = component.get("v.taskList");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var Paginationlist = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                Paginationlist.push(sObjectList[i]);
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.PaginationList', Paginationlist);
    }
})