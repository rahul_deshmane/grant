({
    doInit : function(component, event, helper) {
        helper.doInitHelper(component, event);
    },

    handleSelectCard : function(component, event, helper) {
        component.set("v.showEIA",false);
        console.log('---->', event.currentTarget.name);
        let selectedValue = event.currentTarget.name;
        if(selectedValue!='Waste')
        {
          component.set("v.showWaste",false);
            component.set("v.showEIA",true);
            console.log('Calling Waste form',component.get("v.showWaste"));
        helper.handleSelectCardHelper(component, event, selectedValue);
        }
        else
        {
           
             component.set("v.showWaste",true);
            component.set("v.showEIA",false);
             console.log('Calling Waste form',component.get("v.showWaste"),component.get("v.pickListValues"));
        }
    },
})