({
    doInitHelper : function(component, evnt) {
        console.log('--Init-->');
        let action = component.get("c.getApplicationTypes");
        action.setParams({
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                let res = response.getReturnValue();
                if(res.success && res.pickListResult != null) {
                    component.set('v.pickListValues', res.pickListResult);
                    
                    console.log('@@@res.pickListResult'+JSON.stringify(res.pickListResult));
                }
            } else {
                //Error handling
            }
        });
        $A.enqueueAction(action);
    },

    handleSelectCardHelper : function(component, event, selectedVal) {
        component.set("v.selectedLicense", selectedVal);
    }
})