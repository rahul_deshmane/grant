({
    hideConfirmDialog : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	},
    
    generateDocument : function(component, event, helper) {
        helper.generateDocumentHelper(component, event);
    }
})