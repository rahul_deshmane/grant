({
    generateDocumentHelper : function(component, event) {
        component.set("v.showSpinner", true);
        let action = component.get("c.generateDocumentFile");
        action.setParams({
            "docId" : component.get("v.recordId"),
            "docType" : "Approval_Grant_Document"
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                console.log('Response : ', response.getReturnValue());
                if(response.getReturnValue().isSuccess) {
                    this.showToastMessage(component, event, "Success!" , response.getReturnValue().message, "success");
                } else {
                    this.showToastMessage(component, event, "Error!" , "An error occured while generating the document!", "error");
                }
                component.set("v.showSpinner", false);
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            } else {
                console.log('Error : ', state.getError());
                this.showToastMessage(component, event, "Error!" , state.getError(), "error");
                $A.get("e.force:closeQuickAction").fire();
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    showToastMessage : function(component, event, title, message, type) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})