@isTest
public class AssessmentDocumentTriggerTest {
   
    @isTest
    public static void AssessmentDocumentTest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Custom Customer Community User'];
        List<User> user = [SELECT ProfileId, UserType,ContactId, LanguageLocaleKey, EmployeeNumber FROM User WHERE ContactId != null 
                          AND isActive = true AND ProfileId =: p.Id];
        
       	Contact cont = new Contact();
        cont.FirstName='TestFirst';
        cont.LastName='TestLast';
        insert cont;
      
		Id RTId = Schema.SObjectType.Assessment_Document__c.getRecordTypeInfosByDeveloperName().get('Waste_Application_Tracking').getRecordTypeId();
		  Assessment_Document__c assessmentDoc = (Assessment_Document__c)TestDataFactory.createSObject('Assessment_Document__c', new Map<String,Object>{
              'Assessment_Document_Title__c' => 'Assessment Document Test',
              'Document_Review_Status__c' => 'Draft',
              'Driver_Name__c' => user[0].ContactId,
              'Application_Owner__c'=> user[0].id,
              'RecordTypeId' => RTId
        });
        	Contact con = new Contact();
            con.FirstName='Test1First';
            con.LastName='Test1Last';
            insert con;
        
            assessmentDoc.Driver_Name__c = con.Id;
            update assessmentDoc;
       		
     
        
       
    }
}