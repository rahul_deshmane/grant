@isTest
public class Conga_GenerateDocumentTest {
    
    @testSetup static void testData() {
        List<User> user = [SELECT ProfileId, UserType, LanguageLocaleKey, ContactId, EmployeeNumber FROM User WHERE Profile.name ='DWER Waste carrier' and isActive = true LIMIT 3];
        
        Contact conRec = new Contact();
        conRec.Phone ='1111111189';
        conRec.LastName = 'Test';
        conRec.Department= 'Salesforce';
        conRec.Email='test@gmail.com';
        insert conRec;
        List<vlocity_ps__Application__c> applicationRecs = TestDataFactory.createSObjectList('vlocity_ps__Application__c', new Map<String,Object>{
              'vlocity_ps__Status__c' => 'Draft',
              'Stage__c' => 'Pre-Referral Inquiry',
              'Premise_State__c' => 'NSW',
              'License_Type__c' => 'EIA',
               'Participant__c' =>conRec.Id,
              'Name' => new List<String>{'Test Application','Water Application','EIA Application'}
                  },3);
 
}
    @isTest
    public static void beforeUpdate_isFileRevisionNeededTest(){
        Map<Id,vlocity_ps__Application__c> appRec = new Map<Id,vlocity_ps__Application__c>(); 
       // List<vlocity_ps__Application__c> applicationRec =[SELECT Id from vlocity_ps__Application__c where vlocity_ps__Status__c='Draft'];
        Contact conRec = new Contact();
        conRec.Phone ='1111111189';
        conRec.LastName = 'Test';
        conRec.Department= 'Salesforce';
        conRec.Email='test@gmail.com';
        insert conRec;
        List<vlocity_ps__Application__c> applicationRecs = TestDataFactory.createSObjectList('vlocity_ps__Application__c', new Map<String,Object>{
              'vlocity_ps__Status__c' => 'Draft',
              'Stage__c' => 'Pre-Referral Inquiry',
              'Premise_State__c' => 'NSW',
              'License_Type__c' => 'EIA',
               'Participant__c' =>conRec.Id,
              'Name' => new List<String>{'Test Application','Water Application','EIA Application'}
                  },3);
         String congaTemplateId = '0T_000EA2725240';
         String congaQueryId = '0Q_000EA2806785';
        String sessionId = UserInfo.getSessionId();
        String servUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/29.0/'+UserInfo.getOrganizationId();
        String congaUrl  = 'https://composer.congamerge.com/composer8/index.html?'+'sessionId='+ sessionId +'&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8');
        congaUrl +=  '&id=' + applicationRecs[0].Id;
                congaUrl +=  '&queryid=[Application]' + congaQueryId;
                congaUrl +=  '&defaultPDF=1';
                congaUrl +=  '&templateid=' + congaTemplateId;                   
                congaUrl +=  '&OFN=' + 'TestFile' + '_';
                congaUrl +=  '&APIMODE=1';
                congaUrl +=  '&SC1=SalesforceFile&SC0=1';
                congaUrl +=  '&DS7=11';
        for(vlocity_ps__Application__c app:applicationRecs)
        {
            app.Name ='Testing';
            appRec.put(app.Id, app);
        }
        Test.setMock(HttpCalloutMock.class, new MockClass()); 
        
       
        Conga_GenerateDocument doc =new Conga_GenerateDocument();
        doc.beforeUpdate_isFileRevisionNeeded(applicationRecs, appRec);
        Conga_GenerateDocument.getFileVersion(applicationRecs[0].Id);
        Conga_GenerateDocument.createFile(applicationRecs[0].Id, 'TestFile', congaTemplateId, congaQueryId);
        Conga_GenerateDocument.createFile(applicationRecs[0].Id, 'TestFile');
        Conga_GenerateDocument.createESDFile(applicationRecs[0].Id, 'TestFile', congaTemplateId, congaQueryId);
        Conga_GenerateDocument.createDocument(applicationRecs[0].Id, 'TestFile', congaTemplateId, congaQueryId);
        Conga_GenerateDocument.doCongaCallout(congaUrl, sessionId);
        
    }
}