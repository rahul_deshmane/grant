global class Conga_GrantGenerateDocument {
    global static boolean isFileRevisionNeeded = false;
    global static Set<Id> setFileVers = new Set<Id>();
    
    public static Integer getFileVersion(Id recordId){
        String fileName;
        List<String> versionList;
        
        return 0;
    }
    public void beforeUpdate_isFileRevisionNeeded(List<vlocity_ps__Application__c> applicationListNew, 
                                                  map<Id,vlocity_ps__Application__c> mapApplicationOld){
                                                      
             for(vlocity_ps__Application__c applicationNew : applicationListNew){
                 if(mapApplicationOld.get(applicationNew.Id).Applicant_Email__c != applicationNew.Applicant_Email__c
                   || mapApplicationOld.get(applicationNew.Id).Name != applicationNew.Name
                   || mapApplicationOld.get(applicationNew.Id).Applicant_Address__c != applicationNew.Applicant_Address__c
                   || mapApplicationOld.get(applicationNew.Id).Applicant_Position__c != applicationNew.Applicant_Position__c
                   || mapApplicationOld.get(applicationNew.Id).Applicant_Telephone__c != applicationNew.Applicant_Telephone__c){
                       setFileVers.add(applicationNew.Id);
                       //mapApplicationDocVer.put(applicationNew.Id,true);
                    // applicationNew.Application_File_Version__c = applicationNew.Application_File_Version__c + 1;
                 }                                        
             }
    }
     @future(callout = true)
    public  static void createFile(Id recordId, String FileName){
        
         Integer fileVer=getFileVersion(recordId);
        if(fileVer !=null){
            fileVer +=1; 
        }else{
           fileVer = 0; 
        }
         //STring recordId='a0A2y000009m1FD';
         String congaTemplateId = '0T_000EA2725240';
         String congaQueryId = '0Q_000EA2806785';
         String sessionId = UserInfo.getSessionId(); 
         String servUrl = Url.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/u/29.0/'+UserInfo.getOrganizationId();
         String congaUrl  = 'https://composer.congamerge.com/composer8/index.html?'+'sessionId='+ sessionId +'&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8');
               congaUrl +=  '&id='+recordId;
                congaUrl +=  '&queryid=[Contact]'+congaQueryId;
                congaUrl +=  '&defaultPDF=1';
                congaUrl +=  '&templateid='+congaTemplateId;                   
                congaUrl +=  '&OFN='+FileName+'_';
                congaUrl +=  '&APIMODE=1&DS7=11&SC1=SalesforceFile&SC0=1';
                 /*Queryid -  Specifies if you are using merge fields from conga query
                 defaultPDF -  To generate output in PDf format(True = 1, False = 0)
        //         templateid -  Template Id to generate
        //         OFN -    Sets the Output File Name, overriding the default name of
        //         APIMODE = 1   // Attaches the document to the master object
        //         DS7 = 11  // Background Mode is enabled
        //         SC0 = 1 // Enables Save a Copy.
        //         SC1 = Salesforce File  -  Saves a copy of the merged file in Salesforce Files for the Master Object.
        //         After creating conga url use this in below http method*/

             //doCongaCallout(congaUrl,sessionId);
             //
          Http http = new Http();
             HttpRequest req = new HttpRequest();
             req.setEndpoint(congaUrl);
             req.setMethod('GET');
             req.setTimeout(120000);
             //req.setHeader('Authorization',  'Bearer ' + sessionId);
             // Send the request, and return a response
             System.debug(req);
             HttpResponse res = http.send(req);
               System.debug(res);           
               System.debug('*****Generated Document Id*****'+res.getBody());
    }
    
    public static String createPDF(Id recordId, String fileName, String congaTemplateId, String congaQueryId, Boolean sendEmail) {
        System.debug('createDynamicFile : ' + fileName);
        Integer fileVer = getFileVersion(recordId);
        if(fileVer !=null){
            fileVer +=1; 
        }else{
           fileVer = 0; 
        }
        //STring recordId='a0A2y000009m1FD';
        String sessionId = UserInfo.getSessionId();
        //String congaTemplateId = '0T_001EA2845701';
        //String congaQueryId = '0Q_002EA2051190';
        String servUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/29.0/'+UserInfo.getOrganizationId();
        System.debug('Serve URL : ' + servUrl);
        String congaUrl  = 'https://composer.congamerge.com/composer8/index.html?'+'sessionId='+ sessionId +'&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8');
        //String congaUrl  = 'https://composer.congamerge.com?'+'sessionId='+ sessionId +'&serverUrl='+EncodingUtil.urlEncode(servUrl, 'UTF-8');
                congaUrl +=  '&id=' + recordId;
                congaUrl +=  '&queryid=[GB]' + congaQueryId;
                congaUrl +=  '&defaultPDF=1';
                congaUrl +=  '&templateid=' + congaTemplateId;                   
                congaUrl +=  '&OFN=' + fileName + '_';
                congaUrl +=  '&APIMODE=1';
                congaUrl +=  '&SC1=SalesforceFile&SC0=1';
                congaUrl +=  '&DS7=11';



        //doCongaCallout(congaUrl,sessionId);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(congaUrl);
        req.setMethod('GET');
        req.setTimeout(120000);
        //req.setHeader('Authorization',  'Bearer ' + sessionId);
        // Send the request, and return a response
        System.debug(req);
        HttpResponse res = http.send(req);
        System.debug(res);
        System.debug('*****Generated Document Id*****'+res.getBody());
        if(res.getStatusCode() == 200) {
            String docId = res.getBody();
            List<ContentVersion> docs = [SELECT Id, Document_Type__c, ContentDocumentId FROM ContentVersion WHERE Id =: docId];
            if(fileName != null && !String.isEmpty(fileName) && fileName.contains('ESD')) {
                if(docs != null && !docs.isEmpty()) {
                    ContentVersion cont = new ContentVersion(Id = docs[0].Id, Document_Type__c = 'ESD');
                    update cont;
                }
            }
            return res.getBody();
        } else {
            return 'There was an error while generating document!';
        }
    }

    //This method is being used in Lightning quick actions of Assessment Document object
    @AuraEnabled
    public static string createESDFile(Id recordId, String fileName, String congaTemplateId, String congaQueryId) {
        Conga_GrantGenerateDocument.createFile(recordId, fileName, congaTemplateId, congaQueryId);
        return 'Document generated successfully!';
    }

    
    @future(callout = true)
    public static void createFile(String recordId, String fileName, String congaTemplateId, String congaQueryId) {
        String res = Conga_GrantGenerateDocument.createPDF(recordId, fileName, congaTemplateId, congaQueryId, false);
    }

    @future(callout = true)
    public static void createDocument(String recordId, String fileName, String congaTemplateId, String congaQueryId) {
        String res = Conga_GrantGenerateDocument.createPDF(recordId, fileName, congaTemplateId, congaQueryId, false);
        System.debug('Result : ' + res);
    }

    //This method is used in Javascript buttons in Assessment Document object
    webservice static String createDynamicFile(Id recordId, String fileName, String congaTemplateId, String congaQueryId){
        return Conga_GrantGenerateDocument.createPDF(recordId, fileName, congaTemplateId, congaQueryId, false);
    }

    @future(callout = true)
    public static void doCongaCallout(String congaUrl, String sessionId){
             Http http = new Http();
             HttpRequest req = new HttpRequest();
             req.setEndpoint(congaUrl);
             req.setMethod('GET');
             req.setTimeout(120000);
             //req.setHeader('Authorization',  'Bearer ' + sessionId);
             // Send the request, and return a response
             HttpResponse res = http.send(req);
               System.debug(res);           
               System.debug('*****Generated Document Id*****'+res.getBody());
    }
}