public without sharing class  GrantFetchTaskContoller {
 
    @AuraEnabled
    public static List<Task> getAllTasks(){
        Id currentUserId = UserInfo.getUserId();
        
   /*    List<Task> taskList= [SELECT Id,createddate,Subject,OwnerId,status,ActivityDate,Application__r.Application_Number__c FROM Task where CreatedDate = LAST_N_DAYS:5
                and OwnerId =: currentUserId and status!='Completed' and What.Type='vlocity_ps__Application__c' ORDER By createddate DESC LIMIT 4];*/
        
         List<Task> taskList= [SELECT Id,createddate,Subject,OwnerId,status,ActivityDate,Application__r.Application_Number__c,What.Type FROM Task where 
         OwnerId =: currentUserId and status IN ('Additional Info Required','Not Started','EPA Recommendation required') and What.Type!=null order by ActivityDate desc];
                              // and What.Type='vlocity_ps__Application__c' ORDER By createddate DESC LIMIT 4];
        system.debug('Tasks records*****'+taskList);
        return taskList;
    }
    
    @AuraEnabled
    public static List<Task> getAllCaseTasks(Id caserecordId)
    {
        
        return [SELECT Id,Subject,Owner.Name,Status,EIA_stage_level__c,ActivityDate,createdDate
                FROM Task WHERE EIA_stage_level__c != null and what.Type='Case' and WhatId=:caserecordId ORDER By createdDate DESC];       
        
    }
    @AuraEnabled
    public static PersonaToStageMap__mdt getPersona(Id caseRec)
    {
        Case caseRef=[Select Id,Action_Plan__c,Application_License_Type__c from Case where Id=:caseRec];
        system.debug('Case details*********'+caseRef);
        List<PersonaToStageMap__mdt> personaStageList=[SELECT Persona__c
               FROM PersonaToStageMap__mdt where EIA_Stage__c=:caseRef.Action_Plan__c and License_Type__c =:caseRef.Application_License_Type__c]; 
        system.debug('All persona******'+personaStageList);
       if(personaStageList !=null && personaStageList.size()>0)
        return personaStageList[0]; 
        else
         return null;
        
    }
    
     @AuraEnabled
    public static Case getAppType(Id caseRec)
    {
        Case caseRef=[Select Id,Action_Plan__c,Primary_Application__r.License_Type__c from Case where Id=:caseRec];
        return caseRef;
    }
    
     @AuraEnabled
    public static List<Task> getAllApplicationTasks(Id appId)
    {
        Id caserecordId = [select Id from Case where Primary_Application__c =: appId limit 1].Id;
        return [SELECT Id,Subject,Owner.Name,Status,EIA_stage_level__c
                FROM Task WHERE EIA_stage_level__c != null and what.Type='Case' and WhatId=:caserecordId];       
        
    }
    
   @AuraEnabled    
public static Id createNewTask(String docType, String fileName, String base64Data, String contentType, String subject,Id caseRec,String priority, String status, string description, 
                                     List<String> assignTo, date dueDate ) 
   {   
 
       List<Task> insertTask= createTasks(subject,caseRec, priority, status, description,assignTo, dueDate );
       List<Task> t=[SELECT Id, createdDate from task where Id IN:insertTask ORDER BY createdDate DESC ];
       List <Id> taskList= new List<Id>();
       If(t.size() > 0){
       for(Task tObj:t)
         taskList.add(tObj.Id);
           system.debug('New tasks created******' +insertTask);
       
      }
       
       //system.debug('All values coming from cmp********'+fileName +base64Data    +contentType +docType +tasksId);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
         ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVer.PathOnClient = fileName; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = fileName; // Display name of the files
        conVer.VersionData = EncodingUtil.base64Decode(base64Data); // converting your binary string to Blog
        conVer.Document_Type__c=docType;
        insert conVer;
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        system.debug('Content Document Id***'+conDoc);
        List<ContentDocumentLink> cdls= new List<ContentDocumentLink>();
        for(Id tRef: taskList){
        //Create ContentDocumentLink
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = tRef; // you can use objectId,GroupId etc
        cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDe.Visibility = 'AllUsers';
            cdls.add(cDe);
        }
        system.debug('All files******'+ cdls);
        if(cdls.size()>0)
        insert cdls;
       system.debug('Files added*****'+cdls);
        return conDoc;

   }  
  
@AuraEnabled    
public static List<Task> createTasks( String subject,Id caseRec,String priority, String status, string description, 
                                     List<String> assignTo, date dueDate ) 
   {   
 List<Task> insertTask=New List<Task> ();   
Case caseRef=[Select Id, Primary_Application__r.ownerId,Action_Plan__c,Primary_Application__c,Primary_Application__r.Applicant_Email__c from Case where Id=:caseRec limit 1];   
       If(!assignTo.isEmpty())
       {     
           for(String t:assignTo)
           {   
               Task taskObj=New Task(); 
               taskObj.Subject=subject;   
               taskObj.WhatId= caseRec;   
               taskObj.Description=description;  
               taskObj.Priority= priority; 
               taskObj.Status= status;   
               taskObj.ActivityDate= dueDate;
               
               taskObj.EIA_stage_level__c=caseRef.Action_Plan__c ;  
               taskObj.Application__c=caseref.Primary_Application__c;
               if(t=='Proponent')        
               {   
                   taskObj.Assigned_To_Proponent__c=true; 
                   taskObj.OwnerId=caseRef.Primary_Application__r.ownerId;
                   taskObj.Task_Assignee_Email__c = caseref.Primary_Application__r.Applicant_Email__c;
               }     
               else{      
                   
                   for(QueueSObject queueID : [Select Queue.Id, Queue.Name, Queue.Type ,  Queue.Email
                                               from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name =:t])
                   {
                       System.debug('queueID'+queueID);  
                       if (queueID !=null && queueID.id   != null)
                       {
                           taskObj.OwnerId= queueID.Queue.Id;
                           taskObj.Task_Assignee_Email__c = queueID.Queue.Email;
                           
                       }
                   }
                   
               }     
               
               insertTask.add(taskObj);   
               system.debug('New Task creation*********'+insertTask); 
           }   
       }       
       if(!insertTask.isEmpty() && insertTask.size()!=null)      
       {         
           insert insertTask;   
            
       }    
       return insertTask;
  }
}