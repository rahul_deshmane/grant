public with sharing class Grant_ApplicationListViewController {
    @AuraEnabled
    public static string getRecords(String recLimit) {
        List<RecordWrapper> wrapperList = new List<RecordWrapper>();
        String currentUserId = UserInfo.getUserId();
        String queryString = 'SELECT Name,Id, Recordtype.Name,Application_Title__c,License_Type__c, vlocity_ps__Status__c, Stage__c, LastModifiedDate FROM vlocity_ps__Application__c WHERE (vlocity_ps__Status__c != ' + '\'Rejected\'' + ' AND vlocity_ps__Status__c != ' + '\'Withdrawn\') AND OwnerId = ' + '\'' + currentUserId + '\'' + 'ORDER By LastModifiedDate DESC LIMIT ' + recLimit;
        List<vlocity_ps__Application__c> recList = Database.query(queryString); 
        for(vlocity_ps__Application__c app : recList) {
            RecordWrapper wrapper = new RecordWrapper();
            wrapper.appId = app.Id;
            wrapper.title = app.Name;
            wrapper.type = app.License_Type__c;
            wrapper.status = app.vlocity_ps__Status__c;
            wrapper.stage = app.Stage__c;
            wrapper.recordtype=app.Recordtype.Name;
            wrapper.lastModifiedDate = app.LastModifiedDate.formatGMT('MMM d, yyyy');
            system.debug('All Values'+wrapper);
            wrapperList.add(wrapper);
        }
        return JSON.serialize(wrapperList);
    }

    @AuraEnabled
    public static string getGrantRecords(String recLimit) {
        List<RecordWrapper> wrapperList = new List<RecordWrapper>();
        String currentUserId = UserInfo.getUserId();
        

        String queryString = 'SELECT Name,Id,Project_Title__c,Status__c, LastModifiedDate FROM GrantApplication__c WHERE  CreatedById= ' + '\'' + currentUserId + '\'' + 'ORDER By LastModifiedDate DESC LIMIT ' + recLimit;

        List<GrantApplication__c> recList = Database.query(queryString); 
        for(GrantApplication__c app : recList) {
            RecordWrapper wrapper = new RecordWrapper();
            wrapper.appId = app.Id;
            wrapper.title = app.Project_Title__c;
            wrapper.type = 'Grant Application';
            wrapper.status = app.Status__c;
            wrapper.stage = 'Received';
            wrapper.recordtype='Grant App';
            wrapper.lastModifiedDate = app.LastModifiedDate.formatGMT('MMM d, yyyy');
            system.debug('All Values'+wrapper);
            wrapperList.add(wrapper);
        }
        return JSON.serialize(wrapperList);
    }

    @AuraEnabled
    public static List<Contact> getContacts() {
        return [SELECT Id, Name FROM Contact LIMIT 5];
    }
    
    public class RecordWrapper {
        public Id appId;
        public string title;
        public string type;
        public string recordtype;
        public string status;
        public string stage;
        public string lastModifiedDate;
    }


    
    
    @AuraEnabled
    public static List<vlocity_ps__Application__c> getApplications() {
        return [SELECT Name,Id,License_Type__c, Application_Title__c, vlocity_ps__Status__c, Stage__c, LastModifiedDate FROM vlocity_ps__Application__c 
                WHERE vlocity_ps__Status__c ='Pending for EPA Approval' ORDER BY LastModifiedDate DESC ];
    }
}
