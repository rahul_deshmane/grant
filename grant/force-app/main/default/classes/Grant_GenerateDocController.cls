public with sharing class Grant_GenerateDocController {

    @AuraEnabled
    public static string isESDAvailable(String caseId, String formType){
        try {
            List<Case> caseRecord = [SELECT Id, Primary_Application__c FROM Case WHERE Id =:caseId];
            if(caseRecord != null && !caseRecord.isEmpty()) {
                if(caseRecord[0].Primary_Application__c != null) {
                    String applicationId = caseRecord[0].Primary_Application__c;
                    String esdRecordType = Schema.Sobjecttype.Assessment_Document__c.getRecordTypeInfosByName().get('Environmental Review Document').getRecordTypeId();
                    String erdRecordType = Schema.Sobjecttype.Assessment_Document__c.getRecordTypeInfosByName().get('Environmental Scoping Document').getRecordTypeId();
                    List<String> documentRecordTypeIds = new List<String>{esdRecordType, erdRecordType}; 
                    List<vlocity_ps__Application__c> applications = [SELECT Id, (SELECT Id, Document_Review_Status__c, Application__r.Id, RecordType.Name FROM Assessment_Documents__r WHERE RecordTypeId IN :documentRecordTypeIds AND Document_Review_Status__c != 'Draft') FROM vlocity_ps__Application__c WHERE Id =:applicationId];
                    if(applications != null && !applications.isEmpty()) {
                        System.debug('Applications : ' + applications[0].Assessment_Documents__r);
                        if(applications[0].Assessment_Documents__r != null && !applications[0].Assessment_Documents__r.isEmpty()) {
                            return JSON.serialize(applications[0].Assessment_Documents__r);
                        }
                    }
                }
            }
            return '';
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static string generateDocument(String recordId, String fileName, String documentType){
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            List<ESD_Process__mdt> approvalLetterData = [SELECT Id, Conga_Template_Id__c, Conga_Query_Id__c FROM ESD_Process__mdt WHERE DeveloperName =:documentType LIMIT 1];
            String tempId = approvalLetterData[0].Conga_Template_Id__c;
            String queryId = approvalLetterData[0].Conga_Query_Id__c;

            Conga_GrantGenerateDocument.createFile(recordId, fileName, tempId, queryId);
            if(fileName.contains('ESD')) {
                Assessment_Document__c esdDoc = new Assessment_Document__c();
                esdDoc.Id = recordId;
                esddoc.ESD_Assessment_Start_Date__c = System.today();
                update esddoc;

                rm.message = 'ESD document generation is under progress!';
            } else {
                rm.message = 'ERD document generation is under progress!';
            }
            rm.isSuccess = true;
            return JSON.serialize(rm);
        } catch (Exception ex) {
            rm.message = ex.getMessage();
            return JSON.serialize(rm);
        }
    }

    @AuraEnabled
    public static string approveESD(String recordId, String applicationId, String docType){
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            Assessment_Document__c doc = new Assessment_Document__c(Id = recordId, Document_Review_Status__c = 'Approved');
            rm.message = 'ERD Assessment Document has been approved!';
            if(docType == 'ESD') {
                doc.ESD_Approval_Date__c = System.today();
                rm.message = 'ESD Assessment Document has been approved!';
            }
            update doc;
            rm.isSuccess = true;
            return JSON.serialize(rm);
        } catch (Exception ex) {
            rm.message = ex.getMessage();
            return JSON.serialize(rm);
        }
    }

    @AuraEnabled
    public static ResultMessage updatESDDocInitiated(String caseId) {
        System.debug('Case Id : ' + caseId);
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            if(caseId != null && !String.isEmpty(caseId)) {
                Case cs = new Case(Id = caseId, ESD_Document_Initiated__c = true);
                update cs;

                rm.isSuccess = true;
                rm.message = 'ESD Document initiated successfully!';
            } else {
                rm.message = 'Case Id is missing';
            }
            return rm;
        } catch(Exception ex) {
            System.debug('exception ' + ex.getMessage());
            rm.message = ex.getMessage();
            return rm;
        }
    }

    // @AuraEnabled
    // public static ResultMessage updatERDDocInitiated(String applicationId) {
    //     System.debug('application Id : ' + applicationId);
    //     ResultMessage rm = new ResultMessage();
    //     rm.isSuccess = false;
    //     try {
    //         if(applicationId != null && !String.isEmpty(applicationId)) {
    //             List<vlocity_ps__Application__c> applications =  [SELECT Id, (SELECT Id, ESD_Document_Initiated__c FROM Cases__r) FROM vlocity_ps__Application__c WHERE Id =:applicationId];
    //             if(applications != null && !applications.isEmpty() && applications[0].Cases__r != null && !applications[0].Cases__r.isEmpty()) {
    //                 Case cs = applications[0].Cases__r[0];
    //                 cs.ESD_Document_Initiated__c = true;
    //                 update cs;
    //             }
    //             rm.isSuccess = true;
    //             rm.message = 'ESD Document initiated successfully!';
    //         } else {
    //             rm.message = 'Case Id is missing';
    //         }
    //         return rm;
    //     } catch(Exception ex) {
    //         System.debug('exception ' + ex.getMessage());
    //         rm.message = ex.getMessage();
    //         return rm;
    //     }
    // }

    @AuraEnabled
    public static ResultMessage updatERDDocInitiated(String applicationId){
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            if(applicationId != null && !String.isEmpty(applicationId)) {
                vlocity_ps__Application__c app = new vlocity_ps__Application__c(Id = applicationId, ERD_Document_Initiated__c = true);
                update app;

                rm.isSuccess = true;
                rm.message = 'ERD Document initiated successfully!';
            } else {
                rm.message = 'Application id is missing';
            }
            return rm;
        } catch(Exception ex) {
            rm.message = ex.getMessage();
            return rm;
        }
    }

    @AuraEnabled
    public static ResultMessage checkERDStatus(String caseId){
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            List<Case> cases = [SELECT Id, Primary_Application__c FROM Case WHERE Id =:caseId];
            if(cases != null && !cases.isEmpty() && cases[0].Primary_Application__c != null) {
                Id applicationId = cases[0].Primary_Application__c;
                String erdRecordType = Schema.Sobjecttype.Assessment_Document__c.getRecordTypeInfosByName().get('Environmental Review Document').getRecordTypeId();
                List<Assessment_Document__c> assessmentDocs = [SELECT Id, Document_Review_Status__c 
                                                               FROM Assessment_Document__c 
                                                               WHERE Application__c =:applicationId 
                                                               AND RecordTypeId =:erdRecordType 
                                                               ];
                rm.isSuccess = true;
                if(assessmentDocs != null && !assessmentDocs.isEmpty()) {
                    rm.assessmentDocument = assessmentDocs[0];
                }
            }
            return rm;
        } catch (Exception ex) {
            rm.isSuccess = false;
            rm.message = ex.getMessage();
            return rm;
        }
    }

    public static ESD_Process__mdt getESDPRocessData(String devName) {
        List<ESD_Process__mdt> approvalLetterData = [SELECT Id, Conga_Template_Id__c, Conga_Query_Id__c FROM ESD_Process__mdt WHERE DeveloperName =: devName LIMIT 1];
        return approvalLetterData[0];
    }

    @AuraEnabled
    public static ResultMessage generateDocumentFile(String docId, String docType) {
        System.debug('Doc Type : ' + docType);
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            if(docId != null && !String.isEmpty(docId)) {
                ESD_Process__mdt docData = DWER_GenerateESDController.getESDPRocessData(docType);
                String tempId = docData.Conga_Template_Id__c;
                String queryId = docData.Conga_Query_Id__c;

                Conga_GrantGenerateDocument.createDocument(docId, docType, tempId, queryId);
                    
                //-----Capture Assement Document Start Date------// 
                //Assessment_Document__c esdDoc = new Assessment_Document__c();
                //esdDoc.Id = docId;
                //esdDoc.ESD_Approval_Date__c = System.today();
                //update esdDoc;

                rm.isSuccess = true;
                if(docType.contains('Approval')) {
                   rm.message = 'Approval letter generation is under progress!';
                } else if(docType.contains('EPA')) {
                   rm.message = 'EPA document generation is under progress!';
                } else if(docType.contains('ESD')) {
                    rm.message = 'ESD generation is under progress!';
                } else if(docType.contains('Water')) {
                    rm.message = 'Water Application generation is under progress!';
                } else if(docType.contains('Allocation_Plan')) {
                    rm.message = 'Allocation Plan generation is under progress!';
                } else if(docType.contains('Waste_Holder')) {
                    rm.message = 'Waste Holder Document generation is under progress!';
                }
            } else {
               rm.message = 'Record Id is missing!';
            }
            return rm;
        } catch (Exception ex) {
            rm.message = ex.getStackTraceString();
            return rm;
        }
    }

    @AuraEnabled
    public static ResultMessage publishDocument(String docId, String docType){
        ResultMessage rm = new ResultMessage();
        rm.isSuccess = false;
        try {
            Assessment_Document__c esdDoc = new Assessment_Document__c();
            esdDoc.Id = docId;
            esdDoc.Document_Review_Status__c = 'Published for review';
            update esddoc;

            rm.isSuccess = true;
            rm.message = 'Environmantel Review Document has been published successfully!';
            if(docType == 'ESD') {
                rm.message = 'Environmantel Scoping Document has been published successfully!';
            }
            return rm;
        } catch (Exception ex) {
            rm.message = ex.getMessage();
            return rm;
        }
    }

    public Class ResultMessage {
        @AuraEnabled public String message;
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public Assessment_Document__c assessmentDocument = new Assessment_Document__c();
    }
}