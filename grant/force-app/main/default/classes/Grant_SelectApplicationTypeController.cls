public with sharing class Grant_SelectApplicationTypeController {
    
    @AuraEnabled
    public static ResultMessage getApplicationTypes(){
        ResultMessage rm = new ResultMessage();
        rm.success = false;
        try {
            rm.pickListResult = getPicklListValues();
            rm.success = true;
            rm.message = 'Fields fetched successfully';
            return rm;
        } catch (Exception ex) {
            rm.message = ex.getMessage();
            return rm;
        }
    }

    public static List<PickListWrapper> getPicklListValues() {
        List<PickListWrapper> options = new List<PickListWrapper>();
        

        List<Grant__c> grantList=[Select Id,GrantDescription__c,GrantName__c from Grant__c];


        for( Grant__c gl: grantList) {
            options.add(new PickListWrapper(gl.GrantName__c, gl.GrantName__c,gl.Id));
        }
        return options; 
    }

    public class PickListWrapper {
        @AuraEnabled public string label;
        @AuraEnabled public string value;
        @AuraEnabled public string idname;
        public PickListWrapper(String label, String value,String idname) {
            this.label = label;
            this.value = value;
            this.idname = idname;
        }
    }

    public class ResultMessage {
        @AuraEnabled public boolean success;
        @AuraEnabled public string message;
        @AuraEnabled public List<PickListWrapper> pickListResult;

        public ResultMessage() {
            this.success = false;
            this.message = '';
            pickListResult = new List<PickListWrapper>();
        }
    }
}