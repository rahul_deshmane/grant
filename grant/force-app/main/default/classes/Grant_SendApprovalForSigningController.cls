public with sharing class Grant_SendApprovalForSigningController {

    public String redirectURL {get; set;}
    public PageReference pg;
    public String approvalDocId {get; set;}

    public Grant_SendApprovalForSigningController(ApexPages.StandardController stdController) {
        Assessment_Document__c doc = (Assessment_Document__c)stdController.getRecord();
        List<ContentDocumentLink> documentLinks = [SELECT Id, LinkedEntityId, ContentDocument.Title FROM ContentDocumentLink WHERE LinkedEntityId =: doc.Id];
        approvalDocId = null;
        if(documentLinks != null) {
            for(ContentDocumentLink link : documentLinks) {
                if(link.ContentDocument.Title != null && link.ContentDocument.Title.contains('Approval')) {
                    approvalDocId = link.ContentDocumentId;
                }
            }
        }
        if(approvalDocId != null) {
            if(doc != null && doc.Application__c != null) {
                List<vlocity_ps__Application__c> applications = [SELECT Id, Owner.FirstName, Owner.LastName, Applicant_Email__c FROM vlocity_ps__Application__c WHERE Id =:doc.Application__c];
                if(applications != null && !applications.isEmpty()) {
                    redirectURL = Url.getSalesforceBaseUrl().toExternalForm() + '/apex/APXT_CongaSign__apxt_sendForSignature?id=' + doc.Id;
                    redirectURL += '&businessUnit=Grant';
                    redirectURL += '&documentId=' + approvalDocId;
                    redirectURL += '&emailMessage=Please+Sign+Grant+Approval+Document.';
                    redirectURL += '&emailSubject=Grant+Approval+Document';
                   /* redirectURL += '&recipient1email=' + 'rahul.ulhas.deshmane@pwc.com';
                    redirectURL += '&recipient1first=' + applications[0].Owner.FirstName; 
                    redirectURL += '&recipient1last=' + applications[0].Owner.LastName; 
                    redirectURL += '&recipient1name=' + applications[0].Owner.FirstName + '+' + applications[0].Owner.LastName; */
                    redirectURL += '&recipient1language=en-US'; 
                    redirectURL += '&recipient1role=SIGNER';
                }
            }
        }
    }

    public PageReference redirectToSignature() {
        if(approvalDocId != null && redirectURL != null) {
            PageReference pg = new PageReference(redirectURL);
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }
}