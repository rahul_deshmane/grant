public without sharing class helpFullGuideController { 
    @AuraEnabled(cacheable=true)
    public static List<ContentDocument> retriveFiles(){
       Set<Id> contentdocIds = new Set<Id>();
       List<ContentVersion> conVerList = [SELECT Id, Title, FileExtension, ContentDocumentId, Document_Type__c From ContentVersion WHERE Document_Type__c = 'Helpful Guide' ORDER BY Title];
       for(ContentVersion cvObj :conVerList){
           contentdocIds.add(cvObj.ContentDocumentId);
       }
       List<ContentDocument> cdObjList= [select Id,Title from ContentDocument where Id=: contentdocIds];
       if(cdObjList !=null && cdObjList.size()>0)
       return cdObjList;
       else 
       return null;
    }
    
     @AuraEnabled
    public static void getMeetingDetails(String details, String applicationNumber){
        System.debug('details is called');
        Meeting__c meetingref = new Meeting__c();
        meetingref.Request_Details__c = details;
        vlocity_ps__Application__c appObj=[SELECT Id,Owner.Name,Application_Number__c,
        Stage__c from vlocity_ps__Application__c where Application_Number__c =:applicationNumber];
        meetingref.Name = 'Meeting Requested by '+ appObj.Owner.Name;
        
        Group groupObj = [SELECT Id,Type,Name FROM Group WHERE Type = 'Queue' AND Name = 'New Requests'];
        if(groupObj != null){
            meetingref.OwnerId = groupObj.Id;
        }
        insert meetingref;
        List<vlocity_ps__Application__c> appRec=[SELECT Id,Application_Number__c,Stage__c from vlocity_ps__Application__c where Application_Number__c =:applicationNumber];
        system.debug('Application record_____---'+appRec);
        if(appRec.size() >0) {
        for(vlocity_ps__Application__c appRef: appRec){
       
       DWER_DataUtility.createCaseOnMeeting(meetingref.Id, appRef.Id, appRef.Stage__c);
       }
    }
    }
}