@isTest
public class helpFullGuideControllerTest {
    
    @isTest
    public static void retriveFilesTest(){
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content')
        );
        insert contentVersion_1;
        
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        helpFullGuideController.retriveFiles();
    }
    
    @isTest
    public static void getMeetingDetailsTest(){
        
        vlocity_ps__Application__c applicationTest = (vlocity_ps__Application__c)TestDataFactory.createSObject('vlocity_ps__Application__c', new Map<String,Object>{
              'vlocity_ps__Status__c' => 'Draft',
              'Stage__c' => 'Pre-Referral Inquiry',
              'Premise_State__c' => 'NSW',
              'License_Type__c' => 'EIA',
              'Name' => 'Application Test 22/2'
        });
        
        vlocity_ps__Application__c applicationNumber = [select Id,Application_Number__c from vlocity_ps__Application__c where id = : applicationTest.Id];
        //system.debug('Application Number: '+applicationNumber.Application_Number__c);
        Case c = new Case();
        c.Primary_Application__c = applicationTest.Id;
        c.Status = 'Draft';
        c.Origin = 'Web';
        c.Action_Plan__c = 'Pre-Referral Inquiry';
        insert c;
        
        Meeting__c meeting = (Meeting__c)TestDataFactory.createSObject('Meeting__c', new Map<String,Object>{
              'Case__c' => c.Id,
              'Application__c' => applicationTest.Id,
              'Request_Details__c' => 'Request Details'
        });
        
        helpFullGuideController.getMeetingDetails(meeting.Request_Details__c, applicationNumber.Application_Number__c);
    }
}