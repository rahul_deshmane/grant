<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Approval_Document</label>
    <protected>false</protected>
    <values>
        <field>Conga_Query_Id__c</field>
        <value xsi:type="xsd:string">0Q_004EA2840851</value>
    </values>
    <values>
        <field>Conga_Template_Id__c</field>
        <value xsi:type="xsd:string">0T_003EA2856389</value>
    </values>
</CustomMetadata>
