<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Orchestration Item Definition</label>
    <protected>false</protected>
    <values>
        <field>vlocity_ps__ActivateLimit__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ActivateService__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ActivateType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__DefaultActivateLimit__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__DefaultActivateService__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__DefaultExportLimit__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>vlocity_ps__DefaultExportService__c</field>
        <value xsi:type="xsd:string">Orchestration Item Definition Migration</value>
    </values>
    <values>
        <field>vlocity_ps__DefaultImportLimit__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>vlocity_ps__DefaultImportPostProcess__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__DefaultImportService__c</field>
        <value xsi:type="xsd:string">Orchestration Item Definition Migration</value>
    </values>
    <values>
        <field>vlocity_ps__ExportLimit__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ExportService__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ExportType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ImportLimit__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ImportService__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ImportType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__PrimarySObjectType__c</field>
        <value xsi:type="xsd:string">OrchestrationItemDefinition__c</value>
    </values>
    <values>
        <field>vlocity_ps__ValidateService__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>vlocity_ps__ValidateType__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
