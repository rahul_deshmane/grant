import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
export default class DghNavigateToGrantForm extends NavigationMixin(LightningElement)  {
    handleredirectEIAForm(event){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: '/grandportal/s/new-application' 
            }
        })
        .then(generatedUrl => {
            window.open(generatedUrl,"_top");
        });
    }
}