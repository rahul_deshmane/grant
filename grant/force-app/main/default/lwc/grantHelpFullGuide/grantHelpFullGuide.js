import { LightningElement , wire, track} from 'lwc';
// importing Apex class method
import retriveFiles from '@salesforce/apex/helpFullGuideController.retriveFiles';
import sendMeetingDetails from '@salesforce/apex/helpFullGuideController.getMeetingDetails';
import getAllApplications from '@salesforce/apex/DWER_DataUtility.getApplications';
// importing navigation service
import { NavigationMixin } from 'lightning/navigation';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
export default class GrantHelpFullGuide extends NavigationMixin(LightningElement) {
    @track files;
    @track sfdcBaseURL = true;
    @track redirectToApplication;
    @track allApplications;
    @track selectedApplication; 
    @wire(retriveFiles)
    filesData({data, error}) {
        if(data) {
            window.console.log('data ===> '+data);
            this.files = data;
        }
        else if(error) {
            window.console.log('error ===> '+JSON.stringify(error));
        }
    }
    connectedCallback(){
        let baseUrl = this.getBaseUrl();
        this.redirectToApplication = baseUrl+'grandportal/s/available-grants';
        window.console.log('window.location.href ===> '+window.location.href);
        window.console.log('baseUrl ===> '+baseUrl);
        if(window.location.href == baseUrl+'grandportal/s/available-grants' || window.location.href == baseUrl+'grandportal/s/available-grants'){
            this.sfdcBaseURL = false;
        }
        window.console.log('sfdcBaseURL ===> '+this.sfdcBaseURL);
        getAllApplications()
        .then(result => {
           if(result){
               this.allApplications = result;     
           }
        })
        .catch(error => {
            
        });
    }
    getBaseUrl(){
        let baseUrl = 'https://'+location.host+'/';
        return baseUrl;
    }
    get options(){
       // alert(JSON.stringify(this.allApplications));
        var optionsList = [];
        if(this.allApplications){
            this.allApplications.forEach(app => {
                optionsList.push({ "label": app.applicationObj.Application_Number__c+' - '+app.applicationObj.Application_Title__c, "value": app.applicationObj.Application_Number__c });
            });

        }
        window.console.log("All options*********8",optionsList);
        return optionsList;
    }
    handleOptionChange(event){
        this.selectedApplication = event.target.value;
    }
    filePreview(event) {
        console.log('event.currentTarget.dataset.id'+event.currentTarget.dataset.id);
        let baseUrl = this.getBaseUrl();
        console.log('baseURL'+baseUrl);
        // Naviagation Service to the show preview
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',

            attributes: {
                url: baseUrl+'sfc/servlet.shepherd/document/download/'+event.currentTarget.dataset.id
              
            },
            
        });
    }
    /*filePreview(event) {
        console.log('event.currentTarget.dataset.id'+event.currentTarget.dataset.id);
        let baseUrl = this.getBaseUrl();
        // Naviagation Service to the show preview
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',

            attributes: {
               // url: baseUrl+'sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+event.currentTarget.dataset.id
               pageName: 'filePreview'
            },
            state : {
                selectedRecordId: event.currentTarget.dataset.id//file.ContentDocumentId
            }
        }, false );
    }*/

    @track isModalOpen = false;
    @track meetingRequirements;
    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }

    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }

    submitDetails(event){
        this.meetingRequirements = this.template.querySelector('lightning-textarea').value;
        
        window.console.log('meetingRequirements ===> '+this.meetingRequirements + this.selectedApplication);
        
        if(this.selectedApplication){
        sendMeetingDetails({
            details: this.meetingRequirements,
            applicationNumber: this.selectedApplication
        })
        .then(result => {
            // Show success messsage
            window.console.log('result ===> '+result);
          
            this.dispatchEvent(new ShowToastEvent({
                title: 'Success',
                message: 'Meeting Created Successfully.',
                variant: 'success'
            }), );
            this.isModalOpen = false;
        })
        .catch(error => {
            window.console.log('error.message ===> '+error.message);
            this.error = error.message;
        });

    }
    else{

        this.dispatchEvent(new ShowToastEvent({
            title: 'Error!',
            message: 'Please select an application.',
            variant: 'error'
        }), );
     }
     }
     
}