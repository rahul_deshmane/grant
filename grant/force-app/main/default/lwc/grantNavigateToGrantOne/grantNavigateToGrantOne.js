import {  LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
export default class GrantNavigateToGrantOne extends NavigationMixin(LightningElement)  {


    @api CardTitle;


    /***  import { registerListener } from 'c/pubsub'; ***/
    connectedCallback() {
   console.log('This.CardTitle'+this.CardTitle);
    }
    
    handleredirectEIAForm(event){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: '/grandportal/s/grant/'+ this.CardTitle
            }
        })
        .then(generatedUrl => {
            window.open(generatedUrl,"_top");
        });
    }
}