<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_for_job_assignment_to_driver</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification for job assignment to driver</description>
        <protected>false</protected>
        <recipients>
            <field>Driver_Name__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_for_job_assignment_to_driver</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_carrier_when_task_has_been_assigned_to_driver</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to carrier when task has been assigned to driver</description>
        <protected>false</protected>
        <recipients>
            <field>Carrier_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_to_carrier_when_task_is_assigned_to_driver</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_facility_when_assessment_document_status_is_unloaded</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to facility when assessment document status is unloaded</description>
        <protected>false</protected>
        <recipients>
            <field>Carrier_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Facility_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_when_waste_application_status_is_unloaded</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_waste_holder_when_waste_is_unloaded</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to waste holder when waste is unloaded</description>
        <protected>false</protected>
        <recipients>
            <field>Carrier_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Facility_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_when_waste_application_status_is_unloaded</template>
    </alerts>
    <alerts>
        <fullName>Notification_when_application_tracking_form_is_submitted</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification when application tracking form is submitted</description>
        <protected>false</protected>
        <recipients>
            <field>Application_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_when_application_tracking_is_created</template>
    </alerts>
    <alerts>
        <fullName>Notify_the_proponent_ERD_is_approved</fullName>
        <description>Notify the proponent ERD is approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>Environmental_Consultant_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifications_to_Proponent/DWER_Notify_the_proponent_ERD_approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Record_Locked</fullName>
        <description>Lock record for Driver after status is Unloaded.</description>
        <field>Record_Locked__c</field>
        <literalValue>1</literalValue>
        <name>Record Locked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Notification for job assignment to driver</fullName>
        <actions>
            <name>Notification_for_job_assignment_to_driver</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Waste Application Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Given to driver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to carrier when task is assigned to driver</fullName>
        <actions>
            <name>Notification_to_carrier_when_task_has_been_assigned_to_driver</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Form Initiated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to waste holder that waste has been unloaded</fullName>
        <actions>
            <name>Notification_to_waste_holder_when_waste_is_unloaded</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Unloaded</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when application tracking is submitted</fullName>
        <actions>
            <name>Notification_when_application_tracking_form_is_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Waste Application Tracking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Tracking Form Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify environmental review document is out for public review</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Sent for Public Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify the proponent ERD approved</fullName>
        <actions>
            <name>Notify_the_proponent_ERD_is_approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Non_Urgent_Email_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Read Only After Status Unloaded</fullName>
        <actions>
            <name>Record_Locked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Assessment_Document__c.Document_Review_Status__c</field>
            <operation>equals</operation>
            <value>Unloaded</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
