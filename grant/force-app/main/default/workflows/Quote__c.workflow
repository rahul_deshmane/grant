<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_carrier_when_new_quote_is_requested</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to carrier when new quote is requested</description>
        <protected>false</protected>
        <recipients>
            <field>Quote_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_to_carrier_when_new_quote_has_been_requested</template>
    </alerts>
    <alerts>
        <fullName>Notification_when_quote_has_been_accepted_rejected</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification when quote has been accepted/rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Quote_Primary_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_when_quote_has_been_accepted_rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Status_Update</fullName>
        <field>Status__c</field>
        <literalValue>Pending for Approval</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_On_Final_Rejection</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update On Final Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_on_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>Status Update on Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_on_Final_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Update on Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Notification to carrier when new quote has been requested</fullName>
        <actions>
            <name>Notification_to_carrier_when_new_quote_is_requested</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Quote_Primary_Email__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when quote has been accepted%2Frejected</fullName>
        <actions>
            <name>Notification_when_quote_has_been_accepted_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification when quote has been rejected</fullName>
        <actions>
            <name>Notification_when_quote_has_been_accepted_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
