<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Automatic_Reminder_To_Proponent</fullName>
        <description>Automatic Reminder To Proponent</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifications_to_Proponent/DWER_Automatic_Reminder_To_Proponent</template>
    </alerts>
    <alerts>
        <fullName>Get_Task_Advice</fullName>
        <description>Get Task Advice</description>
        <protected>false</protected>
        <recipients>
            <recipient>Environmental_Consultant_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/DWER_Communities_Changed_Password_Email</template>
    </alerts>
    <alerts>
        <fullName>Get_Task_Advice_for_State_Govt_Partner</fullName>
        <description>Get Task Advice for State Govt Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_External_body/DWER_Get_Adivice</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_more_info</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification for more info</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifications_to_Proponent/DWER_Notification_to_proponent_requesting_response</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Delegated_Officer</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to Delegated Officer</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_Officer/Notification_to_delegated_officer</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_commonwealth</fullName>
        <description>Notification to commonwealth</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_External_body/Notification_to_Commonweath</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_compliance_officer</fullName>
        <description>Notification to compliance officer</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_Officer/DWER_Notification_to_compliance_officer</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_compliance_officer_to_respond_on_EPA</fullName>
        <description>Notification to compliance officer to respond on EPA</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_Officer/Notification_to_compliance_officer_to_respond_to_EPA</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_driver_that_task_has_been_assigned</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to driver that task has been assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_to_driver_that_task_has_been_assigned</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_driver_when_task_has_been_assigned</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to driver when task has been assigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Waste_Application_Notification/Notification_to_driver_that_task_has_been_assigned</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_member_of_public</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to member of public</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_External_body/DWER_Notification_to_member_of_public</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_ministry_one</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to ministry one</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_External_body/Notification_to_ministry_one</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_state_government_partner</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notification to state government partner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_Email_Notifi_to_External_body/Notification_to_state_government_partner</template>
    </alerts>
    <alerts>
        <fullName>Notify_environmental_review_document_is_out_for_public_review</fullName>
        <ccEmails>lakhani.chetan1998@gmail.com</ccEmails>
        <description>Notify environmental review document is out for public review</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_notifications/DWER_Notify_environmental_review_document_is_out_for_public_review</template>
    </alerts>
    <alerts>
        <fullName>Notify_environmental_review_document_is_out_for_public_review_final</fullName>
        <description>Notify environmental review document is out for public review final</description>
        <protected>false</protected>
        <recipients>
            <field>Task_Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DWER_notifications/DWER_Notify_environmental_review_document_is_out_for_public_review</template>
    </alerts>
    <fieldUpdates>
        <fullName>vlocity_ps__TaskCompletedDateUpdate</fullName>
        <field>vlocity_ps__CompletedDate__c</field>
        <formula>Today()</formula>
        <name>Task Completed Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vlocity_ps__TaskCompletedDateUpdate2</fullName>
        <field>vlocity_ps__CompletedDate__c</field>
        <name>Task Completed Date Update 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Automatic reminder to proponent before due date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Response__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Automatic_Reminder_To_Proponent</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notification for more info</fullName>
        <actions>
            <name>Notification_for_more_info</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Assigned_To_Proponent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule sends email to proponent when task status is Additional Info required and task is assigned to proponent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to commonwealth</fullName>
        <actions>
            <name>Notification_to_commonwealth</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Common Wealth One</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to delegated officer</fullName>
        <actions>
            <name>Notification_to_Delegated_Officer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Delegated Officer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to driver that task has been assigned</fullName>
        <actions>
            <name>Notification_to_driver_that_task_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Notification_to_driver_when_task_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Notify Driver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to member of public</fullName>
        <actions>
            <name>Notification_to_member_of_public</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Notify Member of Public</value>
        </criteriaItems>
        <description>This rule sends email to member of public when task status is Notify member of public.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to member of public when task is assigned to MOP</fullName>
        <actions>
            <name>Notification_to_Delegated_Officer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Member Of Public</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to ministry one to respond to EPA</fullName>
        <actions>
            <name>Notification_to_ministry_one</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Ministry One</value>
        </criteriaItems>
        <description>This rule sends email to ministry one so that he approve/reject EPA.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notification to state government partner</fullName>
        <actions>
            <name>Notify_environmental_review_document_is_out_for_public_review</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Notify Govt Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Non_Urgent_Email_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify compliance officer</fullName>
        <actions>
            <name>Notification_to_compliance_officer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Assessment_Document_Record_Type__c</field>
            <operation>equals</operation>
            <value>Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Non_Urgent_Email_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_to_compliance_officer</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Task.ActivityDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Request compliance officer to respond on EPA</fullName>
        <actions>
            <name>Notification_to_compliance_officer_to_respond_on_EPA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Compliance Officer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request state government to comment on EPA</fullName>
        <actions>
            <name>Notification_to_state_government_partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Additional Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>State Gov Partner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vlocity_ps__Task Completed Date Update Rule</fullName>
        <actions>
            <name>vlocity_ps__TaskCompletedDateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vlocity_ps__Task Completed Date Update Rule 2</fullName>
        <actions>
            <name>vlocity_ps__TaskCompletedDateUpdate2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
